#!/usr/bin/env zsh
## configuration file for devuan-minimal-live blend -- Chimaera release


## 
## definition of package sets
##

### base package set
minimal_base_packages+=(
	acpi
	acpi-support-base
	acpid
	acpi-fakekey
	at
	bash
	bash-completion
	bc
	busybox
	console-common
	console-data
	console-setup
	console-setup-linux
	coreutils
	dash
	dc
	dialog
	discover
	dselect
	file
	groff-base
	$grubversion
	info
	kbd
	laptop-detect
	locales
	lsof
	syslinux
	util-linux-locales
	zsh
)

## admin package set 
minimal_admin_packages+=(
	cryptsetup
	cryptsetup-bin
	cryptsetup-initramfs
	cryptsetup-run
	debootstrap
	dosfstools
	entr
	gddrescue
	gdisk
	htop
	iftop
	iotop
	iw
	lvm2
	mdadm
	mtr-tiny
	multitail
	ncdu
	parted 
	pciutils
	psmisc
	sudo
	time
	usbutils
	wavemon 
	wireless-tools
	wpasupplicant
	
)

## editors package set 
minimal_editors_packages+=(
	ed
	nano
	vim-tiny
	zile
)


## networking package set 
minimal_net_packages+=(
	bind9-host
#	bittornado	### not in chimaera
	bti
	ceni
	curl
	dnsutils
	edbrowse
	fetchmail
	ftp-ssl			# in daedalus ftp-ssl or tnftp replaces ftp. lftp gets some votes locally.
	geoip-bin
	haveged
	irssi
	lftp
	links2
	lrzsz
	mcabber
	minicom
	mosh
	msmtp
	mutt
	netcat-openbsd
	net-tools
	nfacct
#	nrss	# not in beowulf. Use rsstail instead.
	openssh-client
	openssh-server
	procmail
	rfkill
	rsstail
#	oysttyer	# use bti instead
	rsync
#	setnet  # use ascii deb package for beowulf and chimaera
	telnet
	traceroute
	transmission-cli
	w3m
	wget
	whois
)


## productivity package set
minimal_prod_packages+=(
	abook
	apcalc
	aspell
	aspell-en
#    calcurse		### not in daedalus. Maybe todoman or w2do?
    clex
    dvtm
	fbi
    fbterm
    ghostscript
    gnupg
	gnupg2
	gnupg-agent
    mc
    parallel
	poppler-utils
	rpl
	rename
    sc
    screen
    taskwarrior
    tmux
    w2do
)

## programming package set
minimal_devel_packages+=(
	tcc
	libc6-dev
	m4
	patch
	perl
	perl-base
	perl-modules-5.36
#	python2.7-minimal
 	python3.11-minimal
	gawk
	guile-3.0
	guile-3.0-libs
)

## games package set 
minimal_games_packages+=(
	bastet
    bombardier
    bsdgames
    cavezofphear
    crawl
    curseofwar
    empire
    freesweep
    gnuchess
    greed
    matanza
    moria
    nethack-console
#    ninvaders		### not in daedalus. Replaced with open-invaders
    omega-rpg
    open-invaders
    pacman4console
    pente
    sudoku
)

## accessibility package set 
minimal_a11y_packages+=(
	beep
    brltty
	espeak
    espeakup
    yasr
)


## multimedia package set 
minimal_media_packages+=(
	alsa-utils
	caca-utils
	fmtools
#    hasciicam	# Not in daedalus
    imagemagick
    jhead
    moc
#    radio		# Not in beowulf. Replaced with fmtools.
    sox	
)

## misc package set
minimal_misc_packages+=(
	cmatrix
	cowsay
	eject
	figlet
	fortunes-min
	fortune-mod
	gpm
	man-db
	manpages
	manpages-dev
	mlocate
#	termsaver		### not in chimaera
	toilet
	toilet-fonts
	ttyrec	
	unzip
)

### now we include all the package sets
base_packages+=(
	$minimal_base_packages
	$minimal_admin_packages
	$minimal_editors_packages
	$minimal_net_packages
	$minimal_prod_packages
	$minimal_devel_packages
#	$minimal_games_packages
	$minimal_a11y_packages
	$minimal_media_packages
	$minimal_misc_packages
)

#### TODO: REPLACE twidge (twitter client)
#### ADDED: geoip-bin

extra_packages+=(
	atmel-firmware
	bluez-firmware
	dahdi-firmware-nonfree
	firmware-amd-graphics
	firmware-atheros
	firmware-bnx2
	firmware-bnx2x
	firmware-brcm80211
	firmware-cavium
	firmware-intel-sound
	firmware-iwlwifi
	firmware-libertas
	firmware-linux-free
	firmware-linux-nonfree
	firmware-misc-nonfree
	firmware-myricom
	firmware-netxen
	firmware-qlogic
	firmware-realtek
	firmware-ti-connectivity
	firmware-zd1211
	refractainstaller-base
	refractasnapshot-base
)


purge_packages=(
	elinks
	elinks-data
	liberror-perl
	libfsplib0
	libtre5
	openntpd
	tasksel
	tasksel-data
)

custom_deb_packages+=(
	memtest86+_5.31b+dfsg-4_${arch}.deb
	setnet_0.4+devuan1_all.deb
#	live-boot-doc_20221008~fsr1_all.deb
#	live-boot-initramfs-tools_20221008~fsr1_all.deb
#	live-boot_20221008~fsr1_all.deb

)


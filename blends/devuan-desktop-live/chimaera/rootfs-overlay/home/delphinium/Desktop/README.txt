
                   Delphinium GNU/Linux 0.0.2 "chimaera" *
                
            Welcome to the reliable world of Delphinium GNU/Linux

   Delphinium offers an extensive collection of original and modified
   Debian as well as Devuan-specific packages. It is a complete 
   Operating System (OS) for your computer. And it is free as in 'freedom'.

   CONTENTS:
     * Introduction
     * Installation
     * Getting Additional Software
     * Report a Bug

   Introduction
   ============

   An Operating System (OS) is a set of programs that provide an interface 
   to the computer's hardware. Resource, device, power, and memory management 
   belong to the OS. The core of the OS in charge of operating the circuitry 
   and managing peripherals is called the kernel. The default flavor of
   Delphinium comes with Linux. Most of the basic operating system tools come
   from the GNU project; hence the name GNU/Linux.

   Installation
   ============

   You can install Delphinium GNU/Linux either as a dual (or multiple) boot 
   alongside your current OS or as the only OS on your computer.

   You can start the installation program easily by double clicking the icon
   on the Desktop named "Install Delphinium".

   Getting Additional Software
   ===========================

   After installing or upgrading, Delphinium's packaging system can use CDs, 
   DVDs, local collections or networked servers (FTP, HTTP) to automatically 
   install software from .deb packages. This is done preferably with the 
   'apt' or 'aptitude' programs.

   You can install packages from the commandline using apt-get. For example, 
   if you want to install the packages 'openssh-client' and 'xlennart', you 
   can give the command:

       apt-get install openssh-client xlennart

   Note that you don't have to enter the complete path or the '.deb' 
   extension. Apt will figure this out itself.

   Or use aptitude for a full screen interactive selection of available 
   Delphinium packages.

   Software can also be installed using the Synaptic graphical interface.
   
   If you have a local .deb package, GDebi can resolve its dependencies and
   install everything for you.

   Report a Bug
   ============

   This is an official release of the Delphinium system. Please report any 
   bugs you find to our github repository at
   https://git.devuan.org/frangamer1892/delphinium-sdk/issues.


Delphinium GNU/Linux
========

Delphinium GNU/Linux is a simple distro based in Devuan, featuring the MATE desktop environment.

Releases at: https://gnlug.org/pub/isos/delphinium/ (user delphinium, password delphinium)

![Screenshot Sys](https://git.devuan.org/frangamer1892/delphinium-sdk/raw/branch/master/screenshot_sys.png)
![Screenshot Tux](https://git.devuan.org/frangamer1892/delphinium-sdk/raw/branch/master/screenshot_tux.png)

## Known issues with the latest release

- None! If you happen to find any, please report them over on the Issues tab.

## Requirements

delphinium-sdk is designed to be used interactively from a terminal.
It requires the following packages to be installed in addition to the
[dependencies required for libdevuansdk](https://git.devuan.org/frangamer1892/libdevuansdk/src/branch/master/README.md#requirements).

`sudo` permissions are required for the user that is running the build.

### Devuan

```
xorriso squashfs-tools live-boot syslinux-common debootstrap
```

## Initial setup

After cloning the delphinium-sdk git repository, enter it and issue:

```
git submodule update --init --recursive --checkout
```

### Updating

To update delphinium-sdk, go to the root dir of the git repo and issue:

```
git pull && git submodule update --init --recursive --checkout
```

## Quick start

Edit the `config` file to match your needs. Sensible defaults are
already there. Then run zsh. To avoid issues, it's best to start a
vanilla session, without preloaded config files so it doesn't cause
issues with libdevuansdk/delphinium-sdk functions.

```
zsh -f
source sdk
```

Now is the time you choose the OS, a blend, and architecture.

### Currently supported distros

* `delphinium`

Example:
```
load delphinium delphinium-desktop-live amd64
```

Once initialized, you can run the helper command:

```
build_iso_dist
```

The image will be automatically built for you. Once finished, you will be
able to find it in the `dist/` directory in delphinium-sdk's root.
